1. Создать класс Product со следующими полями

id: UUID
name: String
description: String
category: ENUM // one of COSMETICS, FOOD, CHEMICAL, TECHNIC
manufactureDateTime: LocalDateTime
manufacturer: String
hasExpiryTime: boolean
stock: int

   Реализовать метод toString

2. Cоздать таблицу product. Таблица product может хранить все поля класса Product

3. Создать класс ProductRepositoryImpl, который представляет CRUD репозиторий к таблице product

Класс ProductRepositoryImpl реализует интерфейс ProductRepository


Конструктор класса ProductRepositoryImpl:

ProductRepositoryImpl(SessionFactory sessionFactory),
где SessionFactory - класс из пакета Hibernate (org.hibernate.SessionFactory)

Ссылка на интерфейс ProductRepository: link

Критерии приемки

Предоставить PR, в котором присутствует реализация класса ProductRepositoryImpl.
